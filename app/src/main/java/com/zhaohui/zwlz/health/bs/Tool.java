package com.zhaohui.zwlz.health.bs;

import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;
import com.luckycatlabs.sunrisesunset.dto.Location;
import com.zhaohui.zwlz.health.model.UserLocal;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 工具辅助类
 * Created by zhaohui on 2016/10/15.
 */

public class Tool {
    /**
     * 根据经度、纬度、当前时间计算出当前日期正午时间
     *
     * @param latitude  纬度
     * @param longitude 经度
     * @param date      当前日期
     * @return 当前日期正午时间
     */
    public Calendar riZhongCalculator(double latitude, double longitude, Calendar date) {
        Location location = new Location(latitude, longitude);
        SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, date.getTimeZone());
        Calendar sr = calculator.getOfficialSunriseCalendarForDate(date);
        Calendar ss = calculator.getOfficialSunsetCalendarForDate(date);
        int r = (int) ((ss.getTimeInMillis() - sr.getTimeInMillis()) / 2000);
        sr.add(Calendar.SECOND, r);
        return sr;
    }

    /**
     * 根据当前北京时间、当天正午时间计算出本地时间
     *
     * @param bj 当前北京时间
     * @param rz 当天正午时间（用北京时区）
     * @return 本地时间偏差秒数
     */
    public int localTime(Calendar bj, Calendar rz) {
        GregorianCalendar r12 = (GregorianCalendar) bj.clone();
        r12.set(GregorianCalendar.HOUR_OF_DAY, 12);
        r12.set(GregorianCalendar.MINUTE, 0);
        r12.set(GregorianCalendar.SECOND, 0);
        GregorianCalendar l = (GregorianCalendar) bj.clone();
        int d = (int) ((r12.getTimeInMillis() - rz.getTimeInMillis()) / 1000);
        //l.add(GregorianCalendar.SECOND, d);
        return d;
    }

    /**
     * 根据当前时间、UserLocal 获得当地信息字符串
     *
     * @param c    当前时间
     * @param ul   本地信息偏差
     * @param zwlz 子午流注信息
     * @return 格式化字符串
     */
    public String formatUserLocal(Calendar c, UserLocal ul, Zwlz zwlz) {
        StringBuffer sb = new StringBuffer();
        //
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        c.add(GregorianCalendar.SECOND, ul.getDiffTime());
        String ls = df.format(c.getTime());
        int h = c.get(Calendar.HOUR_OF_DAY);
        String jl = zwlz.getJingLuo(h);
        String sc = zwlz.getShiChen(h);
        sb.append("城市[").append(ul.getCityName()).append("]当地时间[").append(ls).append("]时间差[").append(ul.getDiffTime()).append("s]时辰[").append(sc).append("]经络[")
                .append(jl).append("]经度[").append(ul.getLongitude()).append("]纬度[").append(ul.getLatitude()).append("]");
        //
        return sb.toString();
    }
}
