package com.zhaohui.zwlz.health.bs;

import android.content.Context;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;

/**
 * Created by zhaohui on 2016/11/17.
 */

public class LocalInfo {
    private static LocalInfo localInfo = null;
    Context mContext;
    boolean init = false;
    //
    private AMapLocationClient mLocationClient;
    //声明AMapLocationClientOption对象
    private AMapLocationClientOption mLocationOption;
    //声明定位回调监听器
    private AMapLocationListener mLocationListener;

    public LocalInfo(Context context) {
        mContext = context;
    }

    public void startLocation() {
        mLocationClient.startLocation();
    }

    public void init(final LocalInfoCallback localInfoCallback) {
        if (!init) {
            //初始化定位
            mLocationClient = new AMapLocationClient(mContext);
            //初始化AMapLocationClientOption对象
            mLocationOption = new AMapLocationClientOption();
            //设置定位模式为AMapLocationMode.Battery_Saving，低功耗模式。
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
            //获取一次定位结果：
            // 该方法默认为false。
            mLocationOption.setOnceLocation(true);
            //获取最近3s内精度最高的一次定位结果：
            // 设置setOnceLocationLatest(boolean b)接口为true，启动定位时SDK会返回最近3s内精度最高的一次定位结果。如果设置其为true，setOnceLocation(boolean b)接口也会被设置为true，反之不会，默认为false。
            mLocationOption.setOnceLocationLatest(true);
            //设置是否允许模拟位置,默认为false，不允许模拟位置
            mLocationOption.setMockEnable(true);
            //给定位客户端对象设置定位参数
            mLocationClient.setLocationOption(mLocationOption);
            mLocationListener = new AMapLocationListener() {
                @Override
                public void onLocationChanged(AMapLocation aMapLocation) {
                    localInfoCallback.onLocationChanged(aMapLocation);
                }
            };
            mLocationClient.setLocationListener(mLocationListener);
            init = true;
        }
    }

    public static interface LocalInfoCallback {
        public void onLocationChanged(AMapLocation aMapLocation);
    }
}
