package com.zhaohui.zwlz.health.model;

import java.util.GregorianCalendar;

/**
 * 用户本地信息
 * Created by zhaohui on 2016/11/17.
 */

public class UserLocal {
    private String cityName;
    private String cityCode;
    private double latitude;
    private double longitude;
    private int diffTime;
    private GregorianCalendar saveTime;

    /**
     * 县市名称
     */
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * 县市代码
     */
    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    /**
     * 纬度
     */
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * 经度
     */
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * 时差（单位秒）
     */
    public int getDiffTime() {
        return diffTime;
    }

    public void setDiffTime(int diffTime) {
        this.diffTime = diffTime;
    }

    /**
     * 数据生成时间
     */
    public GregorianCalendar getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(GregorianCalendar saveTime) {
        this.saveTime = saveTime;
    }
}
