package com.zhaohui.zwlz.health.module;

import com.zhaohui.zwlz.health.bs.Tool;
import com.zhaohui.zwlz.health.bs.Zwlz;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by zhaohui on 2016/11/16.
 */
@Module
public class AppModule {
    @Provides
    @Singleton
    Tool provideTool() {
        return new Tool();
    }

    @Provides
    @Singleton
    Zwlz provideZwlz() {
        return new Zwlz();
    }
}
