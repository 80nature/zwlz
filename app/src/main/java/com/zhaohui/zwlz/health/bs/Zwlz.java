package com.zhaohui.zwlz.health.bs;

/**
 * 子午流注
 * 子时 (23点至1点)，胆经最旺。
 * 丑时 (1点至3点)，肝经最旺。
 * 寅时 (3点至5点)，肺经最旺。
 * 卯时 (5点至7点)，大肠经最旺。
 * 辰时 (7点至9点)，胃经最旺。
 * 巳时 (9点至11点)，脾经最旺。
 * 午时 (11点至13点)，心经最旺。
 * 未时 (13点至15点)，小肠经最旺。
 * 申时 (15点至17点)，膀胱经最旺。
 * 酉时 (17点至19点)，肾经最旺。
 * 戌时 (19点至21点)，心包经最旺。
 * 亥时 (21点至23点)，三焦经最旺
 * Created by zhaohui on 2016/10/15.
 */

public class Zwlz {
    //时辰
    final public static String[] SHICHEN = {"子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"};
    //时辰对应的经络
    final public static String[] JINGLUO = {"胆", "肝", "肺", "大肠", "胃", "脾", "心", "小肠", "膀胱", "肾", "心包", "三焦"};

    //
    //final public static String EMSG="错误的数据";
    public String getShiChen(int localHour) {
        return SHICHEN[getIndex(localHour)];
    }

    public String getJingLuo(int localHour) {
        return JINGLUO[getIndex(localHour)];
    }


    /**
     * 根据当地时间小时获取时辰 经络索引
     *
     * @param localHour 当地时间中的小时
     * @return 时辰 经络索引
     */
    private int getIndex(int localHour) {
        int i = -1;
        if (localHour >= 0 && localHour <= 23) {
            switch (localHour) {
                case 23:
                case 0:
                    i = 0;
                    break;
                case 1:
                case 2:
                    i = 1;
                    break;
                case 3:
                case 4:
                    i = 2;
                    break;
                case 5:
                case 6:
                    i = 3;
                    break;
                case 7:
                case 8:
                    i = 4;
                    break;
                case 9:
                case 10:
                    i = 5;
                    break;
                case 11:
                case 12:
                    i = 6;
                    break;
                case 13:
                case 14:
                    i = 7;
                    break;
                case 15:
                case 16:
                    i = 8;
                    break;
                case 17:
                case 18:
                    i = 9;
                    break;
                case 19:
                case 20:
                    i = 10;
                    break;
                case 21:
                case 22:
                    i = 11;
                    break;
            }

        }
        return i;
    }
}
