package com.zhaohui.zwlz.health.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.zhaohui.zwlz.health.Injection;
import com.zhaohui.zwlz.health.R;
import com.zhaohui.zwlz.health.component.DaggerIAppComponent;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View {
    @BindView(R.id.bLocalTime)
    Button bLocalTime;
    @BindView(R.id.etShow)
    EditText etShow;
    MainActivityContract.Presenter mPresenter;
    boolean isActive = false;
    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setPresenter(MainActivityContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        ButterKnife.bind(this);
        DaggerIAppComponent.builder().build().inject(this);
        //
        mPresenter = new ActivityPresenter(Injection.provideUseCaseHandler(),
                "MainActivity", this, Injection.provideTool(), Injection.provideZwlz(),
                Injection.provideSeachLocal(getApplicationContext()));
    }

    public void onClickLocalTime(View view) {

        //启动定位
        mPresenter.start();
    }

    @Override
    public void showMessage(String msg, boolean add) {
        if (add) {
            etShow.getText().append("\n");
        } else {
            etShow.getText().clear();
        }
        etShow.getText().append(msg);
    }
    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }
}
