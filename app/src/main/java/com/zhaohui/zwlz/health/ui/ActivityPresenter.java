package com.zhaohui.zwlz.health.ui;

import com.zhaohui.zwlz.health.bs.Tool;
import com.zhaohui.zwlz.health.bs.Zwlz;
import com.zhaohui.zwlz.health.comm.UseCase;
import com.zhaohui.zwlz.health.comm.UseCaseHandler;
import com.zhaohui.zwlz.health.model.UserLocal;
import com.zhaohui.zwlz.health.uc.SeachLocal;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by zhaohui on 2016/11/18.
 */

public class ActivityPresenter implements MainActivityContract.Presenter {
    private final UseCaseHandler useCaseHandler;
    private MainActivityContract.View view;
    private String viewId;
    //
    private Tool tool;
    private Zwlz zwlz;
    private SeachLocal seachLocal;

    public ActivityPresenter(UseCaseHandler useCaseHandler, String viewId,
                             MainActivityContract.View view, Tool tool,
                             Zwlz zwlz, SeachLocal seachLocal) {
        this.useCaseHandler = useCaseHandler;
        this.viewId = viewId;
        this.view = view;
        this.tool = tool;
        this.zwlz = zwlz;
        this.seachLocal = seachLocal;
    }


    @Override
    public void start() {
        UserLocal mUserLocal = new UserLocal();
        useCaseHandler.execute(seachLocal, new SeachLocal.RequestValues(mUserLocal),
                new UseCase.UseCaseCallback<SeachLocal.ResponseValue>() {
                    @Override
                    public void onSuccess(SeachLocal.ResponseValue response) {
                        String emsg = response.getErrMsg();
                        if (emsg == null) {
                            UserLocal ul = response.getUserLocal();
                            GregorianCalendar cur = new GregorianCalendar();
                            ul.setSaveTime(cur);
                            //
                            Calendar rz = tool.riZhongCalculator(ul.getLatitude(), ul.getLongitude(), cur);
                            int t = tool.localTime(cur, rz);
                            ul.setDiffTime(t);
                            //
                            String msg = tool.formatUserLocal(cur, ul, zwlz);
                            view.showMessage(msg, false);
                        }
                    }

                    @Override
                    public void onError(SeachLocal.ResponseValue response) {
                        String emsg = response.getErrMsg();
                        if (emsg != null) {
                            view.showMessage(emsg, false);
                        }
                    }
                });
    }
}
