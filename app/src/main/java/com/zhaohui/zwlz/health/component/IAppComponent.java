package com.zhaohui.zwlz.health.component;

import com.zhaohui.zwlz.health.module.AppModule;
import com.zhaohui.zwlz.health.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by zhaohui on 2016/11/16.
 */
@Singleton
@Component(modules = {AppModule.class})
public interface IAppComponent {
    void inject(MainActivity activity);
}
