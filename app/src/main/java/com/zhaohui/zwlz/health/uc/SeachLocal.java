package com.zhaohui.zwlz.health.uc;


import android.util.Log;

import com.amap.api.location.AMapLocation;
import com.zhaohui.zwlz.health.bs.LocalInfo;
import com.zhaohui.zwlz.health.comm.UseCase;
import com.zhaohui.zwlz.health.model.UserLocal;


/**
 * Created by zhaohui on 2016/11/17.
 */

public class SeachLocal extends UseCase<SeachLocal.RequestValues, SeachLocal.ResponseValue> {
    private final LocalInfo mLocalInfo;

    public SeachLocal(LocalInfo localInfo) {
        mLocalInfo = localInfo;
    }

    @Override
    protected void executeUseCase(final RequestValues values) {
        final UserLocal userLocal = values.getUserLocal();
        mLocalInfo.init(new LocalInfo.LocalInfoCallback() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                if (aMapLocation.getErrorCode() == 0) {
                    userLocal.setCityCode(aMapLocation.getCityCode());
                    userLocal.setCityName(aMapLocation.getCity());
                    userLocal.setLatitude(aMapLocation.getLatitude());
                    userLocal.setLongitude(aMapLocation.getLongitude());
                    getUseCaseCallback().onSuccess(new ResponseValue(userLocal));
                } else {
                    String msg = aMapLocation.getErrorCode() + ":" + aMapLocation.getErrorInfo();
                    Log.e("alE:", msg);
                    getUseCaseCallback().onError(new ResponseValue(userLocal, msg, aMapLocation.getErrorCode()));
                }
            }
        });
        mLocalInfo.startLocation();
    }

    public static final class RequestValues implements UseCase.RequestValues {

        private final UserLocal mUserLocal;

        public RequestValues(UserLocal userLocal) {
            mUserLocal = userLocal;
        }

        public UserLocal getUserLocal() {
            return mUserLocal;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue {

        private final UserLocal mUserLocal;
        private final String errMsg;
        private int code;

        public ResponseValue(UserLocal userLocal) {
            mUserLocal = userLocal;
            errMsg = null;
            code = 0;
        }

        public ResponseValue(UserLocal userLocal, String errMsg, int code) {
            mUserLocal = userLocal;
            this.errMsg = errMsg;
            this.code = code;
        }

        public UserLocal getUserLocal() {
            return mUserLocal;
        }

        public String getErrMsg() {
            return errMsg;
        }
    }
}
