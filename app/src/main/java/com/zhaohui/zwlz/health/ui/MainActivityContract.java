package com.zhaohui.zwlz.health.ui;

import com.zhaohui.zwlz.health.comm.BasePresenter;
import com.zhaohui.zwlz.health.comm.BaseView;

/**
 * Created by zhaohui on 2016/11/17.
 */

public interface MainActivityContract {
    interface View extends BaseView<Presenter> {

        /**
         * 显示信息
         *
         * @param msg 信息
         * @param add 是否追加
         */
        void showMessage(String msg, boolean add);


        boolean isActive();
    }

    interface Presenter extends BasePresenter {

    }
}
