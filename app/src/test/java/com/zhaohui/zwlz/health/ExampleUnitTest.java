package com.zhaohui.zwlz.health;

import com.zhaohui.zwlz.health.bs.Tool;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        Tool tool = new Tool();
        GregorianCalendar gc = new GregorianCalendar(2016, 10, 15, 0, 15, 0);
        double latitude = 43.77, longitude = 87.66;
        Calendar rz = tool.riZhongCalculator(latitude, longitude, gc);
        int ds = tool.localTime(gc, rz);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(df.format(gc.getTime()));
        System.out.println(ds);
        assertEquals(4, 2 + 2);
    }
}